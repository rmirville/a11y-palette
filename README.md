# Palette Ally

This tool generates palettes that attempt to meet:

- [WCAG 2.1 AAA](https://www.w3.org/WAI/WCAG22/quickref/?versions=2.1#col_overview&levels=aaa#top) minimum [text contrast](https://www.w3.org/WAI/WCAG21/Understanding/contrast-minimum.html) and [non-text contrast](https://www.w3.org/WAI/WCAG21/Understanding/non-text-contrast.html) requirements
- APCA Readability Criterion's [bronze Level](https://readtech.org/ARC/#bronze) requirements

[WCAG (Web Content Accessibility Guidelines)](https://www.w3.org/WAI/standards-guidelines/wcag/) is an international technical standard for making web content more accessible to people with disabilities.

[APCA (Accessible Perceptual Contrast Algorithm)](https://git.apcacontrast.com/documentation/APCA_in_a_Nutshell) is an experimental system for measuring color contrast that attempts to reflect human perception more accurately than the WCAG's contrast ratio system.

This palette-generator includes [automated tests for conformance](src/modules/palette/helpers/PaletteHelper.test.ts) to the APCA and WCAG color contrast standards.

# Run the project

```sh
npm ci
npm start
```
