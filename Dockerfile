from node:18-alpine AS build

WORKDIR /usr/local/src/app
COPY . .

RUN npm ci
RUN npm run build

FROM httpd:2.4

COPY --from=build /usr/local/src/app/build/ /usr/local/apache2/htdocs
EXPOSE 80
