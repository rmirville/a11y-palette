import type { JestConfigWithTsJest } from 'ts-jest';

const jestConfig: JestConfigWithTsJest = {
	testEnvironment: 'node',
	testMatch: ['**/?(*.)+(spec|test).ts?(x)'],
	preset: 'ts-jest/presets/default-esm',
	moduleNameMapper: {
		'^(\\.{1,2}/.*)\\.js$': '$1',
	},
	transform: {
		'^.+\\.[tj]sx?$': [
			'ts-jest',
			{
				useESM: true,
			},
		],
	},
};
export default jestConfig;
