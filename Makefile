.PHONY run:
run:
	npm run start

.PHONY build:
build:
	npm run build

.PHONY image:
image:
	docker image build -t palette-ally:latest .

.PHONY container:
container: image
	docker container run --rm -itd -p 60080:80 palette-ally:latest

.PHONY test:
test:
	npx react-scripts test
