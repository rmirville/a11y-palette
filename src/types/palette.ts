import { AnyColorLevel } from './color';

export type Palette = {
	base: string;
	colors: Record<AnyColorLevel, string>;
};
