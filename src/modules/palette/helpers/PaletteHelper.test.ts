import { APCAcontrast, sRGBtoY } from 'apca-w3';
import chroma from 'chroma-js';
import { MatcherFunction } from 'expect';
import { AnyColorLevel, colorLevelDisplayMap } from '../../../types/color';
import { Palette } from '../../../types/palette';
import PaletteHelper from './PaletteHelper';

const isPalette = (value: unknown): value is Palette => {
	return (
		typeof value === 'object' &&
		value !== null &&
		value.hasOwnProperty('base') &&
		value.hasOwnProperty('colors')
	);
};

type BaseContrastRequirement = {
	foreground: AnyColorLevel;
	background: AnyColorLevel;
};
type MinContrastRequirement = BaseContrastRequirement & {
	min: number;
};

type MaxContrastRequirement = BaseContrastRequirement & {
	max: number;
};

const toMeetWcagContrastStandard: MatcherFunction<[]> = function (
	actual: unknown
) {
	if (!isPalette(actual)) {
		throw new TypeError('`actual` must be a palette');
	}
	const contrastRequirements: MinContrastRequirement[] = [
		{
			foreground: 'WCAG_2_1_AAA_L100',
			background: 'WCAG_2_1_AAA_BLACK',
			min: 4.5,
		},
		{
			foreground: 'WCAG_2_1_AAA_L300',
			background: 'WCAG_2_1_AAA_BLACK',
			min: 3,
		},
		{
			foreground: 'WCAG_2_1_AAA_L700',
			background: 'WCAG_2_1_AAA_WHITE',
			min: 3,
		},
		{
			foreground: 'WCAG_2_1_AAA_L500',
			background: 'WCAG_2_1_AAA_BLACK',
			min: 4.5,
		},
		{
			foreground: 'WCAG_2_1_AAA_L500',
			background: 'WCAG_2_1_AAA_WHITE',
			min: 4.5,
		},
	];
	for (const { foreground, background, min } of contrastRequirements) {
		const contrast = chroma.contrast(
			actual.colors[foreground],
			actual.colors[background]
		);
		if (contrast < min) {
			return {
				pass: false,
				message: () =>
					`${actual.base} palette has insufficient contrast between ${colorLevelDisplayMap[foreground].label} and ${colorLevelDisplayMap[background].label}\nMinimum: ${min}\nActual: ${contrast}`,
			};
		}
	}
	return {
		pass: true,
		message: () =>
			`${actual.base} palette meets WCAG 2.1 AAA contrast standard`,
	};
};

const toMeetApcaContrastStandard: MatcherFunction<[]> = function (
	actual: unknown
) {
	if (!isPalette(actual)) {
		throw new TypeError('`actual` must be a palette');
	}
	const lightModeContrastRequirements: MinContrastRequirement[] = [
		// siblings
		{
			foreground: 'APCA_L300',
			background: 'APCA_L150',
			min: 15,
		},
		{
			foreground: 'APCA_L450',
			background: 'APCA_L300',
			min: 15,
		},
		{
			foreground: 'APCA_L600',
			background: 'APCA_L450',
			min: 15,
		},
		{
			foreground: 'APCA_L750',
			background: 'APCA_L600',
			min: 15,
		},
		{
			foreground: 'APCA_L900',
			background: 'APCA_L750',
			min: 15,
		},
		// text against white
		{
			foreground: 'APCA_L450',
			background: 'APCA_WHITE',
			min: 45,
		},
		{
			foreground: 'APCA_L600',
			background: 'APCA_WHITE',
			min: 60,
		},
		{
			foreground: 'APCA_L750',
			background: 'APCA_WHITE',
			min: 75,
		},
		{
			foreground: 'APCA_L900',
			background: 'APCA_WHITE',
			min: 90,
		},
		// text against L150
		{
			foreground: 'APCA_L600',
			background: 'APCA_L150',
			min: 45,
		},
		{
			foreground: 'APCA_L750',
			background: 'APCA_L150',
			min: 60,
		},
		{
			foreground: 'APCA_L900',
			background: 'APCA_L150',
			min: 75,
		},
	];
	const darkModeContrastRequirements: MaxContrastRequirement[] = [
		// siblings
		{
			foreground: 'APCA_D150',
			background: 'APCA_BLACK',
			max: -15,
		},
		{
			foreground: 'APCA_D300',
			background: 'APCA_D150',
			max: -15,
		},
		{
			foreground: 'APCA_D450',
			background: 'APCA_D300',
			max: -15,
		},
		{
			foreground: 'APCA_D600',
			background: 'APCA_D450',
			max: -15,
		},
		{
			foreground: 'APCA_D750',
			background: 'APCA_D600',
			max: -15,
		},
		{
			foreground: 'APCA_D900',
			background: 'APCA_D750',
			max: -15,
		},
		// text against black
		{
			foreground: 'APCA_D300',
			background: 'APCA_BLACK',
			max: -30,
		},
		{
			foreground: 'APCA_D450',
			background: 'APCA_BLACK',
			max: -45,
		},
		{
			foreground: 'APCA_D600',
			background: 'APCA_BLACK',
			max: -60,
		},
		{
			foreground: 'APCA_D750',
			background: 'APCA_BLACK',
			max: -75,
		},
		{
			foreground: 'APCA_D900',
			background: 'APCA_BLACK',
			max: -90,
		},
		// text against d150
		{
			foreground: 'APCA_D450',
			background: 'APCA_D150',
			max: -30,
		},
		{
			foreground: 'APCA_D600',
			background: 'APCA_D150',
			max: -45,
		},
		{
			foreground: 'APCA_D750',
			background: 'APCA_D150',
			max: -60,
		},
		{
			foreground: 'APCA_D900',
			background: 'APCA_D150',
			max: -75,
		},
		// text against D300
		{
			foreground: 'APCA_D750',
			background: 'APCA_D300',
			max: -45,
		},
		{
			foreground: 'APCA_D900',
			background: 'APCA_D300',
			max: -60,
		},
	];

	for (const { foreground, background, min } of lightModeContrastRequirements) {
		const sRgb1 = chroma(actual.colors[foreground]).rgb();
		const sRgb2 = chroma(actual.colors[background]).rgb();
		const contrast = APCAcontrast(sRGBtoY(sRgb1), sRGBtoY(sRgb2)) as number;
		if (contrast < min) {
			return {
				pass: false,
				message: () =>
					`${actual.base} palette has insufficient contrast between ${colorLevelDisplayMap[foreground].label} and ${colorLevelDisplayMap[background].label}\nMinimum: ${min}\nActual: ${contrast}`,
			};
		}
	}
	for (const { foreground, background, max } of darkModeContrastRequirements) {
		const sRgb1 = chroma(actual.colors[foreground]).rgb();
		const sRgb2 = chroma(actual.colors[background]).rgb();
		const contrast = APCAcontrast(sRGBtoY(sRgb1), sRGBtoY(sRgb2)) as number;
		if (contrast > max) {
			return {
				pass: false,
				message: () =>
					`${actual.base} palette has insufficient contrast between ${colorLevelDisplayMap[foreground].label} and ${colorLevelDisplayMap[background].label}\nMaximum: ${max}\nActual: ${contrast}`,
			};
		}
	}
	return {
		pass: true,
		message: () => `${actual.base} palette meets APCA contrast standard`,
	};
};
expect.extend({
	toMeetWcagContrastStandard,
	toMeetApcaContrastStandard,
});

describe('PaletteHelper', () => {
	describe('colorToPalette', () => {
		describe('wcag21aaa: standard', () => {
			describe('L500', () => {
				it('converts grayscale to #797979', () => {
					const color = '#000000';
					const palette = PaletteHelper.colorToPalette({ color });
					expect(palette.base).toBe(color);
					expect(palette.colors.WCAG_2_1_AAA_WHITE).toBe('#ffffff');
					expect(palette.colors.WCAG_2_1_AAA_BLACK).toBe('#000000');
					expect(
						chroma.contrast(
							chroma(palette.colors.WCAG_2_1_AAA_L500),
							chroma(palette.colors.WCAG_2_1_AAA_WHITE)
						)
					).toBeLessThan(4.666666666666667);
					expect(
						chroma.contrast(
							chroma(palette.colors.WCAG_2_1_AAA_L500),
							chroma(palette.colors.WCAG_2_1_AAA_WHITE)
						)
					).toBeGreaterThan(4.5);
				});
			});
		});
		describe('for a random color', () => {
			for (let i = 0; i < 256 * 64; i++) {
				it('creates a palette compliant with WCAG 2.1 AAA contrast standards', () => {
					const color = chroma.random().hex();
					const palette = PaletteHelper.colorToPalette({ color });
					expect(palette).toMeetWcagContrastStandard();
				});
				it('creates a palette compliant with APCA Bronze contrast standards', () => {
					const color = chroma.random().hex();
					// console.debug('color:', color);
					const palette = PaletteHelper.colorToPalette({ color });
					expect(palette).toMeetApcaContrastStandard();
				});
			}
		});
	});
	it('should be defined', () => {
		expect(PaletteHelper).toBeDefined();
	});
});
