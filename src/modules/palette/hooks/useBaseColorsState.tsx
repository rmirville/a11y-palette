import { useState } from 'react';

const useBaseColorsState = (
	initialState: string[],
): [string[], (baseColors: string[]) => void, () => void] => {
	const [state, setState] = useState(initialState);
	const setBaseColorsState = (baseColors: string[]) =>
		setState([...baseColors] || state);
	const resetBaseColorsState = () => setState([...initialState]);
	return [state, setBaseColorsState, resetBaseColorsState];
};

export default useBaseColorsState;
