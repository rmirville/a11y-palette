import { useState } from 'react';
import { ColorContrastStandard } from '../../../types/color';

const useColorContrastStandardState = (
	initialState: ColorContrastStandard,
): [
	ColorContrastStandard,
	(standard: ColorContrastStandard) => void,
	() => void,
] => {
	const [state, setState] = useState(initialState);
	const setColorContrastStandardState = (standard: ColorContrastStandard) =>
		setState(standard || state);
	const resetColorContrastStandardState = () => setState(initialState);
	return [
		state,
		setColorContrastStandardState,
		resetColorContrastStandardState,
	];
};

export default useColorContrastStandardState;
