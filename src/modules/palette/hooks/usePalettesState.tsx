import { useState } from 'react';
import { Palette } from '../../../types/palette';
import PaletteHelper from '../helpers/PaletteHelper';

const usePalettesState = (
	initialBaseColorsState: string[]
): [Palette[], (baseColors: string[]) => void, () => void] => {
	const [state, setState] = useState(
		PaletteHelper.colorsToPalettes({
			colors: initialBaseColorsState,
		})
	);
	const setPalettesState = (baseColors: string[]) => {
		setState(PaletteHelper.colorsToPalettes({ colors: baseColors }));
	};
	const resetPalettesState = () =>
		setState(
			PaletteHelper.colorsToPalettes({
				colors: initialBaseColorsState,
			})
		);
	return [state, setPalettesState, resetPalettesState];
};

export default usePalettesState;
