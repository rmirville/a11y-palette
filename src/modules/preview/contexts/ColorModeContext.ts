import { createContext } from 'react';
import { ColorMode } from '../../../types/color';

const defaultColorMode: ColorMode = 'light';

export const ColorModeContext = createContext<ColorMode>(defaultColorMode);
