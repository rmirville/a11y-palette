export const jsonDebug = (label: string, value: Object) =>
	console.debug(label, JSON.stringify(value, null, 2));
